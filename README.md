## trace
自用rpc通信框架中用于追踪rpc调用的库，需要搭配其他两个项目使用

单独使用引入：
```java
<dependency>
  <groupId>com.lowdad</groupId>
  <artifactId>trace</artifactId>
  <version>1.0.1</version>
</dependency>
```

## 使用
原理是利用logback中的mdc存储机制，在请求中加入traceId属性并打印出来
单条调用链享用同一个traceId,当然有更好更重的方式(springcloud全家桶中的trace)

编写logback-spring.xml
springboot-web中已加入了相关的依赖
```java
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
    <appender name="ConsoleAppender" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <charset>UTF-8</charset>
            <Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%level] [%thread] [%logger] [trace_id=%mdc{trace_id:-0}] %msg %n</Pattern>
        </encoder>
    </appender>
    <springProfile name="local">
        <root level="INFO">
            <appender-ref ref="ConsoleAppender"/>
        </root>
    </springProfile>
</configuration>
```
关键词为 trace_id,无rpc调用时默认为0

结合文件打印和日志收集组成日志系统，这里不赘述，如果你有好的方案欢迎提issue


## License MIT