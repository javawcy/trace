package com.lowdad.trace;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author lowdad
 */
public class TraceIdUtils {

    public static String getTraceId() {
        return Long.toString(Math.abs(ThreadLocalRandom.current().nextLong()), 16);
    }

}