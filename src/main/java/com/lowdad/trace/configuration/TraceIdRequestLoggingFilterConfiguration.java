package com.lowdad.trace.configuration;

import com.lowdad.trace.TraceIdRequestLoggingFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lowdad
 */
@Configuration
@ConditionalOnWebApplication
public class TraceIdRequestLoggingFilterConfiguration {
    @Bean
    public TraceIdRequestLoggingFilter createTraceIdMDCFilter() {
        return new TraceIdRequestLoggingFilter();
    }

}
